# MTK-MCCLIM-DOT

## Unreleased

## v0.2.1 - March 08, 2022

Fix bug when using layout overrides to provide edges.

## v0.2.0 - March 01, 2022

Use Shasht to parse JSON instead of Jsown. Safety 0 bad.

Add `:layout-override` and `make-layout-override` to make sure node (and maybe
edge) positions do not change between calls to `format-graph-from-roots`.

## v0.1.0 - October 26, 2021

First release!

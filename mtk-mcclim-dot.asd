(in-package #:asdf-user)

(defsystem "mtk-mcclim-dot"
  :license "MIT"
  :version (:read-file-form "version.lisp-expr")
  :description "Support for using DOT based graph layout engines."
  :depends-on ("mtk-mcclim-dot/core" "mtk-mcclim-dot/graphviz")
  :in-order-to ((test-op (test-op "mtk-mcclim-dot/test"))))

(defsystem "mtk-mcclim-dot/core"
  :license "MIT"
  :version (:read-file-form "version.lisp-expr")
  :description "Core DOT routines."
  :depends-on ("mcclim" "mcclim-bezier" "cl-dot" "alexandria" "split-sequence" "parse-number"
               "closer-mop")
  :components ((:file "package")
               (:file "core" :depends-on ("package"))))

(defsystem "mtk-mcclim-dot/graphviz"
  :license "MIT"
  :version (:read-file-form "version.lisp-expr")
  :description "Interface to graphviz via an external process."
  :depends-on ("mtk-mcclim-dot/core" "shasht" "uiop")
  :components ((:file "graphviz")))

(defsystem "mtk-mcclim-dot/test"
  :license "MIT"
  :version (:read-file-form "version.lisp-expr")
  :description "Test suite for mtk-mcclim-dot."
  :depends-on ("mtk-mcclim-dot/core"
               "mtk-mcclim-dot/graphviz"
               "mcclim-raster-image"
               "fiveam")
  :pathname "test"
  :components ((:file "package")
               (:file "smoke" :depends-on ("package"))
               (:file "edge-labels" :depends-on ("package")))
  :perform (test-op (operation component)
             (uiop:symbol-call '#:mtk-mcclim-dot.test '#:run-tests)))

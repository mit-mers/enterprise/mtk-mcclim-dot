;;;; mtk-mcclim-dot.test package
;;;;
;;;; This file is part of mtk-mcclim-dot. See README.md and LICENSE for more
;;;; information.

(defpackage #:mtk-mcclim-dot.test
  (:use
   #:cl
   #:fiveam)

  (:export
   #:run-tests))

(in-package #:mtk-mcclim-dot.test)

(def-suite* :mtk-mcclim-dot)

(defun run-tests ()
  (unless (run! :mtk-mcclim-dot)
    (error "Tests failed")))
